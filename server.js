const express = require('express')
const app = express()
const http = require('http').Server(app)

const io = require('socket.io')(http)

const users = []

app.use(express.static(__dirname))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/main.html')
})

io.on('connection', socket => {
  io.emit('update users', users)

  socket.on('setName', (user, fn) => {
    const userExist = users.find(record => record.name === user.name)

    if (!userExist) {
      users.unshift(user)
      io.emit('update users', users)
      fn(true)
    } else {
      fn(false)
    }
  })

  socket.on('disconnect', id => {
    let index
    for (let i = 0; i < users.length; i++) {
      if (users[i].id === id) {
        index = i
      }
    }

    users.splice(index, 1)
    io.emit('update users', users)
  })

  socket.on('broadcast', msg => {
    io.emit('broadcast', msg)
  })
})

http.listen(9999, () => {
  console.log('listening on 9999...')
})
