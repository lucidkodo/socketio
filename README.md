# socketio

My practice with socketio

---

1. `git clone git@gitlab.com:lucidkodo/socketio.git && cd socketio`

2. `yarn install`

3. `nodemon server.js` (_Make sure you have [nodemon](https://nodemon.io/) installed_) or `node server.js`

4. Open [localhost:9999](localhost:9999)
